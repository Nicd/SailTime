#ifndef SYSINFO_H
#define SYSINFO_H

#include <QObject>
#include <sys/sysinfo.h>

#include "sysload.h"

/*
 * This class exists because we cannot pass plain structs onto the QML side,
 * we need to inherit from QObject.
 */
class SysinfoC : public QObject
{
    Q_OBJECT
public:
    explicit SysinfoC(const struct sysinfo* info, QObject* parent = 0);

    /*
     * Loads are reported as unsigned longs in the sysinfo struct, this function
     * will convert them to floats which are usually used for displaying loads.
     */
    static float load2Float(unsigned long load);

    Q_PROPERTY(long uptime READ getUptime)
    Q_PROPERTY(Sysload* loads READ getLoads)
    Q_PROPERTY(unsigned long totalram READ getTotalram)
    Q_PROPERTY(unsigned long freeram READ getFreeram)
    Q_PROPERTY(unsigned long sharedram READ getSharedram)
    Q_PROPERTY(unsigned long bufferram READ getBufferram)
    Q_PROPERTY(unsigned long totalswap READ getTotalswap)
    Q_PROPERTY(unsigned long freeswap READ getFreeswap)
    Q_PROPERTY(unsigned short procs READ getProcs)
    Q_PROPERTY(unsigned long totalhigh READ getTotalhigh)
    Q_PROPERTY(unsigned long freehigh READ getFreehigh)
    Q_PROPERTY(unsigned int mem_unit READ getMemunit)

    long getUptime() const;
    Sysload* getLoads() const;
    unsigned long getTotalram() const;
    unsigned long getFreeram() const;
    unsigned long getSharedram() const;
    unsigned long getBufferram() const;
    unsigned long getTotalswap() const;
    unsigned long getFreeswap() const;
    unsigned short getProcs() const;
    unsigned long getTotalhigh() const;
    unsigned long getFreehigh() const;
    unsigned int getMemunit() const;

signals:

public slots:

private:
    long uptime;                  /* Seconds since boot */
    Sysload* loads;               /* 1, 5, and 15 minute load averages */
    unsigned long totalram;       /* Total usable main memory size */
    unsigned long freeram;        /* Available memory size */
    unsigned long sharedram;      /* Amount of shared memory */
    unsigned long bufferram;      /* Memory used by buffers */
    unsigned long totalswap;      /* Total swap space size */
    unsigned long freeswap;       /* swap space still available */
    unsigned short procs;         /* Number of current processes */
    unsigned long totalhigh;      /* Total high memory size */
    unsigned long freehigh;       /* Available high memory size */
    unsigned int mem_unit;        /* Memory unit size in bytes */
};

#endif // SYSINFO_H
