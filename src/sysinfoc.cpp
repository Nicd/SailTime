#include "sysinfoc.h"
#include <linux/kernel.h>

SysinfoC::SysinfoC(const struct sysinfo* info, QObject* parent) :
    QObject(parent),
    uptime(info->uptime),
    totalram(info->totalram),
    freeram(info->freeram),
    sharedram(info->sharedram),
    bufferram(info->bufferram),
    totalswap(info->totalswap),
    freeswap(info->freeswap),
    procs(info->procs),
    totalhigh(info->totalhigh),
    freehigh(info->freehigh),
    mem_unit(info->mem_unit)
{
    float in_loads[3] = {0.0, 0.0, 0.0};
    in_loads[0] = load2Float(info->loads[0]);
    in_loads[1] = load2Float(info->loads[1]);
    in_loads[2] = load2Float(info->loads[2]);

    this->loads = new Sysload(in_loads, this);
}

float SysinfoC::load2Float(unsigned long load)
{
    // As per http://stackoverflow.com/a/4993273
    return load / ((float) (1 << SI_LOAD_SHIFT));
}

long SysinfoC::getUptime() const
{
    return this->uptime;
}

Sysload* SysinfoC::getLoads() const
{
    return this->loads;
}

unsigned long SysinfoC::getTotalram() const
{
    return this->totalram;
}

unsigned long SysinfoC::getFreeram() const
{
    return this->freeram;
}

unsigned long SysinfoC::getSharedram() const
{
    return this->sharedram;
}

unsigned long SysinfoC::getBufferram() const
{
    return this->bufferram;
}

unsigned long SysinfoC::getTotalswap() const
{
    return this->totalswap;
}

unsigned long SysinfoC::getFreeswap() const
{
    return this->freeswap;
}

unsigned short SysinfoC::getProcs() const
{
    return this->procs;
}

unsigned long SysinfoC::getTotalhigh() const
{
    return this->totalhigh;
}

unsigned long SysinfoC::getFreehigh() const
{
    return this->freehigh;
}

unsigned int SysinfoC::getMemunit() const
{
    return this->mem_unit;
}
