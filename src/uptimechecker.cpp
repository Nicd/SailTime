/*
 * © Mikko Ahlroth 2013–2014
 * SailTime is open source software. For licensing information, please check
 * the LICENCE file.
 */

#include "uptimechecker.h"


UptimeChecker::UptimeChecker(QObject *parent) :
    QObject(parent)
{}

SysinfoC* UptimeChecker::fetchUptime()
{
    struct sysinfo info;
    sysinfo(&info);
    return new SysinfoC(&info, this);
}
