# The name of your app.
# NOTICE: name defined in TARGET has a corresponding QML filename.
#         If name defined in TARGET is changed, following needs to be
#         done to match new name:
#         - corresponding QML filename must be changed
#         - desktop icon filename must be changed
#         - desktop filename must be changed
#         - icon definition filename in desktop file must be changed
TARGET = harbour-sailtime

CONFIG += sailfishapp

HEADERS += src/uptimechecker.h \
    src/sysinfoc.h \
    src/sysload.h

SOURCES += src/harbour-sailtime.cpp \
    src/uptimechecker.cpp \
    src/sysinfoc.cpp \
    src/sysload.cpp

OTHER_FILES += qml/harbour-sailtime.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    qml/pages/SecondPage.qml \
    qml/js/storage.js \
    rpm/harbour-sailtime.spec \
    rpm/harbour-sailtime.yaml \
    harbour-sailtime.desktop \
    qml/pages/AboutPage.qml

