/*
 * © Mikko Ahlroth 2013–2014
 * SailTime is open source software. For licensing information, please check
 * the LICENCE file.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../pages"

CoverBackground {
    id: cover
    property alias cover : cover

    function updateCover(uptime) {
        var strings = formatUptime(uptime.days, uptime.hours, uptime.minutes);

        days.text = strings.days;
        time.text = strings.hours + " " + strings.minutes;
        load1.text = uptime.load1.toFixed(2);
        load5.text = uptime.load5.toFixed(2);
        load15.text = uptime.load15.toFixed(2);
    }

    Column {
        width: cover.width
        spacing: Theme.paddingSmall
        y: Theme.paddingLarge

        Label {
            id: days
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Refreshing..."
            color: Theme.primaryColor
            font.family: Theme.fontFamilyHeading
        }

        Label {
            id: time
            anchors.horizontalCenter: parent.horizontalCenter
            text: ""
            color: Theme.primaryColor
            font.family: Theme.fontFamilyHeading
        }

        Separator {
            x: Theme.paddingLarge
            width: parent.width - Theme.paddingLarge * 2
            horizontalAlignment: Qt.AlignCenter
            color: Theme.highlightColor
        }

        Row {
            width: parent.width - Theme.paddingLarge * 2
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 15

            Label {
                text: "1"
                color: Theme.secondaryColor
                horizontalAlignment: Qt.AlignRight
                width: parent.width / 4
                font.family: Theme.fontFamilyHeading
            }

            Label {
                id: load1
                text: ""
                color: Theme.primaryColor
                horizontalAlignment: Qt.AlignLeft
                width: 3 * parent.width / 4
                font.family: Theme.fontFamilyHeading
            }
        }

        Row {
            width: parent.width - Theme.paddingLarge * 2
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 15

            Label {
                text: "5"
                color: Theme.secondaryColor
                horizontalAlignment: Qt.AlignRight
                width: parent.width / 4
                font.family: Theme.fontFamilyHeading
            }

            Label {
                id: load5
                text: ""
                color: Theme.primaryColor
                horizontalAlignment: Qt.AlignLeft
                width: 3 * parent.width / 4
                font.family: Theme.fontFamilyHeading
            }
        }

        Row {
            width: parent.width - Theme.paddingLarge * 2
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 15

            Label {
                text: "15"
                color: Theme.secondaryColor
                horizontalAlignment: Qt.AlignRight
                width: parent.width / 4
                font.family: Theme.fontFamilyHeading
            }

            Label {
                id: load15
                text: ""
                color: Theme.primaryColor
                horizontalAlignment: Qt.AlignLeft
                width: 3 * parent.width / 4
                font.family: Theme.fontFamilyHeading
            }
        }
    }

    CoverActionList {
        id: coverAction

        CoverAction {
            iconSource: "image://theme/icon-cover-refresh"
            onTriggered: updateDisplay()
        }
    }
}


