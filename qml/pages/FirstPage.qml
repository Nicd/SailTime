/*
 * © Mikko Ahlroth 2013–2014
 * SailTime is open source software. For licensing information, please check
 * the LICENCE file.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    AboutPage {
        id: aboutpage
    }

    id: page

    property alias page : page

    Component.onCompleted: {
        updateDisplay();
    }

    function updatePage(sysinfo, uptime) {
        var strings = formatUptime(uptime.days, uptime.hours, uptime.minutes);

        days.text = strings.days;
        time.text = strings.hours + " " + strings.minutes;

        if (sysinfo.uptime >= uptime.record) {
            recordLabel.text = "Making new record!";
        }
        else {
            var rStrings = formatUptime(uptime.rDays, uptime.rHours, uptime.rMinutes);
            recordLabel.text = "Record: " + rStrings.days + " " + rStrings.hours + " " + rStrings.minutes;
        }


        load1.text = uptime.load1.toFixed(2);
        load5.text = uptime.load5.toFixed(2);
        load15.text = uptime.load15.toFixed(2);


        // Memory bar

        var meminuse = sysinfo.totalram - sysinfo.freeram;

        usedmem.width = (5 * usedmem.parent.width / 6 - Theme.paddingLarge * 2) * (meminuse / sysinfo.totalram);
        freemem.width = (5 * freemem.parent.width / 6 - Theme.paddingLarge * 2) * (sysinfo.freeram / sysinfo.totalram);

        var usedram = scaleMem((sysinfo.totalram - sysinfo.freeram), sysinfo.mem_unit);
        var totalram = scaleMem(sysinfo.totalram, sysinfo.mem_unit);

        memlegend.text = usedram[0].toFixed(2) + " " + usedram[1] + "B/"
                + totalram[0].toFixed(2) + " " + totalram[1] + "B ("
                + (meminuse / sysinfo.totalram * 100).toFixed(2) + " %)";


        // Swap bar

        var swapinuse = sysinfo.totalswap - sysinfo.freeswap;

        usedswap.width = (5 * usedswap.parent.width / 6 - Theme.paddingLarge * 2) * (swapinuse / sysinfo.totalswap);
        freeswap.width = (5 * freeswap.parent.width / 6 - Theme.paddingLarge * 2) * (sysinfo.freeswap / sysinfo.totalswap);

        var usedswap_scale = scaleMem((sysinfo.totalswap - sysinfo.freeswap), sysinfo.mem_unit);
        var totalswap_scale = scaleMem(sysinfo.totalswap, sysinfo.mem_unit);

        swaplegend.text = usedswap_scale[0].toFixed(2) + " " + usedswap_scale[1] + "B/"
                + totalswap_scale[0].toFixed(2) + " " + totalswap_scale[1] + "B ("
                + (swapinuse / sysinfo.totalswap * 100).toFixed(2) + " %)";


        // Processes amount

        procs.text = sysinfo.procs + " processes + threads";
    }


    function setRecordEnabled(enable)
    {
        recordLabel.visible = enable;
    }


    // Scale memory sizes to human readable bytes according to mem_unit
    // Return memory size in bytes and prefix
    function scaleMem(mem, mem_unit) {
        var scaled = mem * mem_unit;

        var size_factor = 1024;
        var size_units = ['', 'ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi', 'Yi'];

        var remainder = 0;

        for (var i = 0; i < size_units.length; ++i) {
            remainder = scaled / Math.pow(size_factor, i);

            if (remainder < size_factor) {
                return [remainder, size_units[i]];
            }
        }

        // If we have passed yottabytes in memory size, just rudely display as
        // YB.
        return [remainder, size_units[size_units.length - 1]];
    }

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            MenuItem {
                text: "About SailTime"
                onClicked: pageStack.push(aboutpage);
            }

            MenuItem {
                text: "Settings"
                onClicked: pageStack.push(secondpage);
            }

            MenuItem {
                text: "Refresh"
                onClicked: updateDisplay();
            }
        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.
        Column {
            id: column
            width: page.width
            spacing: Theme.paddingMedium

            PageHeader {
                title: "Uptime"
            }

            Label {
                id: days
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Refreshing..."
                color: Theme.primaryColor
                font.pixelSize: Theme.fontSizeLarge
            }

            Label {
                id: time
                anchors.horizontalCenter: parent.horizontalCenter
                text: ""
                color: Theme.primaryColor
                font.pixelSize: Theme.fontSizeLarge
            }

            Label {
                id: recordLabel
                anchors.horizontalCenter: parent.horizontalCenter
                text: ""
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeMedium
                visible: false
            }


            PageHeader {
                title: "Load averages"
            }

            Row
            {
                width: parent.width - Theme.paddingLarge * 2
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 18

                Label {
                    text: "1"
                    color: Theme.secondaryColor
                    horizontalAlignment: Qt.AlignRight
                    width: parent.width / 24
                }

                Label {
                    id: load1
                    text: ""
                    color: Theme.primaryColor
                    width: 3 * parent.width / 12
                    horizontalAlignment: Qt.AlignLeft
                }

                Label {
                    text: "5"
                    color: Theme.secondaryColor
                    horizontalAlignment: Qt.AlignRight
                    width: parent.width / 24
                }

                Label {
                    id: load5
                    text: ""
                    color: Theme.primaryColor
                    width: 3 * parent.width / 12
                    horizontalAlignment: Qt.AlignLeft
                }

                Label {
                    text: "15"
                    color: Theme.secondaryColor
                    horizontalAlignment: Qt.AlignRight
                    width: parent.width / 24
                }

                Label {
                    id: load15
                    text: ""
                    color: Theme.primaryColor
                    width: 3 * parent.width / 12
                    horizontalAlignment: Qt.AlignLeft
                }
            }

            PageHeader {
                title: "System information"
            }

            Item {
                height: Theme.fontSizeLarge
                width: parent.width

                Label {
                    id: memlabel
                    text: "Mem"
                    color: Theme.secondaryColor
                    width: parent.width / 6
                    horizontalAlignment: Qt.AlignRight
                }

                Rectangle {
                    anchors.left: memlabel.right
                    anchors.leftMargin: Theme.paddingLarge
                    id: usedmem
                    height: Theme.fontSizeLarge
                    width: 0
                    color: Theme.highlightColor
                }

                Rectangle {
                    anchors.left: usedmem.right
                    id: freemem
                    height: Theme.fontSizeLarge
                    width: 0
                    color: Theme.secondaryHighlightColor
                }

                Label {
                    anchors.left: memlabel.right
                    anchors.leftMargin: Theme.paddingLarge
                    id: memlegend
                    color: "black"
                    width: 5 * parent.width / 6 - Theme.paddingLarge * 2
                    horizontalAlignment: Qt.AlignCenter

                    // Overlay the label on the bar
                    z: 1
                    font.pixelSize: Theme.fontSizeExtraSmall
                }
            }

            Item {
                height: Theme.fontSizeLarge
                width: parent.width

                Label {
                    id: swaplabel
                    text: "Swap"
                    color: Theme.secondaryColor
                    width: parent.width / 6
                    horizontalAlignment: Qt.AlignRight
                }

                Rectangle {
                    anchors.left: swaplabel.right
                    anchors.leftMargin: Theme.paddingLarge
                    id: usedswap
                    height: Theme.fontSizeLarge
                    width: 0
                    color: Theme.highlightColor
                }

                Rectangle {
                    anchors.left: usedswap.right
                    id: freeswap
                    height: Theme.fontSizeLarge
                    width: 0
                    color: Theme.secondaryHighlightColor
                }

                Label {
                    anchors.left: swaplabel.right
                    anchors.leftMargin: Theme.paddingLarge
                    id: swaplegend
                    color: "black"
                    width: 5 * parent.width / 6 - Theme.paddingLarge * 2
                    horizontalAlignment: Qt.AlignCenter

                    // Overlay the label on the bar
                    z: 1
                    font.pixelSize: Theme.fontSizeExtraSmall
                }
            }

            Label {
                id: procs
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
    }
}


